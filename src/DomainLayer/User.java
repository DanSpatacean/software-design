package DomainLayer;

import Presentation.*;
import MapperLayer.*;

public class User 
{
	private String user;
    private String pass;
    
    public User(String user,String pass)
    {
        this.user=user;
        this.pass=pass;
    }
    
    public String getUser()
    {
        return this.user;
    }
    
     public String getPass()
    {
        return this.pass;
    }
}
