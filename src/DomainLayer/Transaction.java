package DomainLayer;

import java.util.Calendar;
import java.util.Date;

public class Transaction 
{
	private int id;
    private int eId;
    private int cId;
    private int acc;
    private String type;
    private int amount;
    private Calendar creationDate;
    
    public void setId(int val) 
    {
        id = val;
    }

    public void setAcc(int val) 
    {
        acc = val;
    }

    public void setEmpId(int str) 
    {
        eId = str;
    }

    public void setCustId(int str) 
    {
        cId = str;
    }

    public void setType(String str) 
    {
        this.type = str;
    }

    public void setAmount(int str) 
    {
        amount = str;
    }

    public void setDate(String str) 
    {
        int y = 0, m = 0, d = 0;
        String delim = "-";
        String[] tokens = str.split(delim);
        for (int i = 0; i < tokens.length; i++) 
        {
            y = m;
            m = d;
            d = Integer.parseInt(tokens[i]);
        }
        creationDate.set(y, m, d);
    }
    
    public int getId() 
    {
        return id;
    }

    public int getEmpId() 
    {
        return eId;
    }

    public int getCustId() 
    {
        return cId;
    }

    public int getAcc() 
    {
        return acc;
    }

    public String getType() 
    {
        return type;
    }

    public int getAmount() 
    {
        return amount;
    }
    
    public String getYear() 
    {
        String date = creationDate.get(Calendar.YEAR) + "" ;
        return date;
    }
    
    public String getMounth() 
    {
        String date = creationDate.get(Calendar.MONTH)+1 + "" ;
        return date;
    }
    
    public String getDay() 
    {
        String date = creationDate.get(Calendar.DATE) + "" ;
        return date;
    }

    public String getCrDate() 
    {
        String date = creationDate.get(Calendar.YEAR) + "-" + (creationDate.get(Calendar.MONTH) + 1) + "-" + creationDate.get(Calendar.DATE);
        return date;
    }

    public String getCrDate2() 
    {
        String date = creationDate.get(Calendar.YEAR) + "-" + (creationDate.get(Calendar.MONTH)) + "-" + creationDate.get(Calendar.DATE);
        return date;
    }
}
