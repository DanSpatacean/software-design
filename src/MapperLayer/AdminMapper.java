package MapperLayer;

import DomainLayer.*;
import java.sql.*;
import javax.swing.*;
import java.util.*;

public class AdminMapper 
{
	private Connection db = null;
    private ResultSet rs = null;
    private PreparedStatement pst = null;
    
    public boolean verifyAdmin(User l) 
    {
        String statement = "select * from admin where user=? and pass=?";
        //System.out.println("user "+l.getUser()+" pass " +  l.getPass());
        try 
        {
            db = Javaconnect.ConnecrDb();

            pst = db.prepareStatement(statement);
            pst.setString(1, l.getUser());
            pst.setString(2, l.getPass());
            rs = pst.executeQuery();

            if (rs.next()) 
            {
                JOptionPane.showMessageDialog(null, "Username and Password are correct");
                rs.close();
                pst.close();
                return true;
            } 
            else 
            {
                //JOptionPane.showMessageDialog(null, "Data nowhere to be found...sorry");
                return false;
            }
        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, e);
        }

        return false;
    }
    
    public ArrayList<Employee> getEmployeeList() 
    {
        ArrayList<Employee> eList = new ArrayList<Employee>();
       
        String sql = "select * from employee";
        try 
        {     
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) 
            {
                Employee e = new Employee("0","0");
                e.setId(rs.getInt("idEmployee"));
                e.setUser(rs.getString("user"));

                e.setPass(rs.getString("pass"));
                e.setName(rs.getString("name"));
                

                eList.add(e);
            }

        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "getEmplList " + e);
        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
     
        return eList;
    }
    
    public void deleteEmployee(int id)
    {
        String sql = "delete from employee where idEmployee=?";
        try 
        {     
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);
            pst.setInt(1,id);
            int rs = pst.executeUpdate();
        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "delete Employee " + e);
            System.out.println("delete Employee " + e);
        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }   
    }
    
    public void createEmployee(int id,String user,String pass,String name)
    {
        String sql = "insert into employee values(?,?,?,?)";
        try 
        {     
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);
            pst.setInt(1,id);
            pst.setString(2, user);
            pst.setString(3, pass);
            pst.setString(4, name);
            int rs = pst.executeUpdate();
        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "create Empolyee " + e);
            System.out.println("create Employee " + e);
        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }  
    }
    
    public void updateEmployee(int id,String user,String pass,String name)
    {
        String sql = "update employee set user=? , pass=? , name=? where idEmployee=?";
        try 
        {     
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);
            
            pst.setString(1, user);
            pst.setString(2, pass);
            pst.setString(3, name);
            pst.setInt(4,id);
            int rs = pst.executeUpdate();
        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "update Employee " + e);
            System.out.println("update Employee " + e);

        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
    }
    
    public ArrayList<Transaction> getEmplTrList() 
    {
      ArrayList<Transaction> accTrList = new ArrayList<Transaction>();
      
      String sql = "select * from transaction";
      try 
      {
          db = Javaconnect.ConnecrDb();
          pst = db.prepareStatement(sql);
          rs = pst.executeQuery();
          
          while (rs.next()) 
          {
              Transaction c = new Transaction();
              c.setId(rs.getInt("idTransaction"));
              c.setEmpId(rs.getInt("eId"));
              c.setCustId(rs.getInt("cId"));
              int tip = rs.getInt("trtid");
              switch (tip) 
              {
                  case 1:
                      c.setType("transfer");
                      break;
                  case 2:
                      c.setType("deposit");
                      break;
                  case 3:
                      c.setType("withdraw");
                      break;
              }
            

              c.setAmount(rs.getInt("amount"));
              c.setDate(rs.getString("date"));

              accTrList.add(c);
          }

      } 
      catch (Exception e) 
      {
          JOptionPane.showMessageDialog(null, "getcList " + e);
      } 
      finally 
      {
          try
          {
              rs.close();
              pst.close();
          } 
          catch (Exception e) 
          {
          }
      }
      
      return accTrList;
  }
    
    
}
